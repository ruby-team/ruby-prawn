#!/usr/bin/ruby

puts 'Building manual...'
require File.expand_path(File.join(__dir__, %w[.. manual contents]))
prawn_manual_document.render_file(File.join(__dir__, %w[.. manual.pdf]))
puts 'The Prawn manual is available at manual.pdf. Happy Prawning!'

